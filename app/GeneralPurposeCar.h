#pragma once

#include "config.h"
#include "PassengerCar.h"


class GeneralPurposeCar : public PassengerCar {
protected:
    char powerType[STRSIZE]{};
public:
    GeneralPurposeCar(const char*, const char*, int, int, float, bool, const char*);
    GeneralPurposeCar();
    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);
    const char *getPowerType() const;
};
