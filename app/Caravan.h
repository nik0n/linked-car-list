#pragma once

#include "config.h"
#include "FreightCar.h"

class Caravan : public FreightCar {
protected:
    char typeOfSale[STRSIZE]{};
public:
    Caravan(const char*, const char*, int, int, float, bool, const char*);
    Caravan();
    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);
    const char *getTypeOfSale() const;
};
