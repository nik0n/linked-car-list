#pragma once
#include <cstdint>

#include "Car.h"
#include "config.h"

class FreightCar : public Car {
protected:
    bool hingedCab;
public:
    FreightCar(ClassName classname, const char*, const char *, int, int, float, bool);
    FreightCar();
    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);
    bool getHingedCab() const;
};
