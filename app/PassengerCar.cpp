#include <iostream>
#include <cstring>

#include "PassengerCar.h"

PassengerCar::PassengerCar(
        ClassName classname,
        const char *brand,
        const char *model,
        int maxSpeed,
        int mass,
        float length,
        bool autopilot
) : Car(classname, brand, model, maxSpeed, mass, length), autopilot(autopilot) {
    classname = PASSENGERCAR;
}

PassengerCar::PassengerCar() {
    classname = PASSENGERCAR;
}

void PassengerCar::print(std::ostream& ostream) const {
    Car::print(ostream);
    ostream << "\n" << "Autopilot: " << autopilot;
}

void PassengerCar::save(std::ofstream& fout) {
    Car::save(fout);
    fout.write(reinterpret_cast<char*>(&autopilot), sizeof(autopilot));
}

void PassengerCar::load(std::ifstream& fin) {
    Car::load(fin);
    fin.read(reinterpret_cast<char*>(&autopilot), sizeof(autopilot));
}

bool PassengerCar::getAutopilot() const {
    return autopilot;
}
