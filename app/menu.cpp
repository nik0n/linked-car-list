#include <iostream>
#include <cstring>
#include <map>

#include "Car.h"
#include "Tractor.h"
#include "Caravan.h"
#include "SportsCar.h"
#include "GeneralPurposeCar.h"
#include "menu.h"
#include "config.h"
#include "utils.h"
#include "List.h"

class CStringComparator {
public:
    bool operator()(const char* first, const char* second) const {
        return std::strcmp(first, second) < 0;
    }
};

using CmdMap = std::map<const char*, void (*)(const char*), CStringComparator>;

List<Car> list;

void help(const char* line) {
    std::cout
        << "add - add new object to the Car list" << std::endl
        << "remove (name) - remove all properties of (brand)" << std::endl
        << "remove all - remove all objects" << std::endl
        << "search (name) - print first properties of (brand)" << std::endl
        << "print - print the Car list" << std::endl
        << "load (filename) - load objects from (filename) into the Car list" << std::endl
        << "save (filename) - save the Car list in (filename)" << std::endl
        << "help - help with commands" << std::endl
        << "exit - exit" << std::endl;
}

void exit(const char* line) {
    std::exit(0);
}

void load(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Error 1: Need filename" << std::endl;
        return;
    }
    std::ifstream fin(line);
    if (!fin.good()) {
        std::cout << "Error 2: Cant open '" << line << "'" << std::endl;
        return;
    }
    fin.close();

    list.loadFrom(line);
}

void save(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Error 1: Need filename" << std::endl;
        return;
    }

    list.saveAll(line);
}

void print(const char* line) {
    list.printAll();
}

void removeObj(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Error 3: Need name" << std::endl;
        return;
    }

    if (std::strcmp(line, "all") == 0) {
        list.removeAll();
    } else {
        list.remove(line);
    }
}

void search(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Error 3: Need name" << std::endl;
        return;
    }

    Car* vehicle = list.search(line);
    if (vehicle == nullptr) {
        std::cout << "Error 4: Vehicle not found." << std::endl;
    } else {
        std::cout << *vehicle;
    }
}

void add(const char* line) {
    Car* vehicle;

    char classname[STRSIZE] = {};
    char brand[STRSIZE] = {};
    char model[STRSIZE] = {};
    int maxSpeed;
    int mass;
    float length;
    std::cout << "Classname (Tractor, Caravan, SportsCar, GeneralPurposeCar): ";
    std::cin.getline(classname, STRSIZE);
    std::cout << "Brand: ";
    std::cin.getline(brand, STRSIZE);
    std::cout << "model: ";
    std::cin.getline(model, STRSIZE);
    std::cout << "maxSpeed: ";
    std::cin >> maxSpeed;
    std::cout << "mass: ";
    std::cin >> mass;
    std::cout << "length: ";
    std::cin >> length;

    if (std::strcmp(classname, "Caravan") == 0 || std::strcmp(classname, "Tractor") == 0) {
        bool hingedCab;
        clearcin();
        std::cout << "hingedCab: ";
        std::cin >> hingedCab;

        if (std::strcmp(classname, "Caravan") == 0) {
            char typeOfSale[STRSIZE];
            std::cout << "typeOfSale: ";
            clearcin();
            std::cin.getline(typeOfSale, STRSIZE);
            vehicle = new Caravan(brand, model, maxSpeed, mass, length, hingedCab, typeOfSale);
        } else {
            char typeOfTrailer[STRSIZE];
            std::cout << "typeOfTrailer: ";
            clearcin();
            std::cin.getline(typeOfTrailer, STRSIZE);

            vehicle = new Tractor(brand, model, maxSpeed, mass, length, hingedCab, typeOfTrailer);
        }
    } else if (std::strcmp(classname, "SportsCar") == 0 || std::strcmp(classname, "GeneralPurposeCar") == 0) {
        bool autopilot;
        std::cout << "autopilot: ";
        std::cin >> autopilot;

        if (std::strcmp(classname, "SportsCar") == 0) {
            char turbineType[STRSIZE];
            std::cout << "turbineType: ";
            std::cin >> turbineType;

            vehicle = new SportsCar(brand, model, maxSpeed, mass, length, autopilot, turbineType);
        } else {
            char powerType[STRSIZE];
            std::cout << "powerType: ";
            std::cin >> powerType;

            vehicle = new GeneralPurposeCar(brand, model, maxSpeed, mass, length, autopilot, powerType);
        }
    } else {
        std::cout << "Unknown class" << std::endl;
    }
    list.append(vehicle);
}

void run_menu() {
    CmdMap commands;
    commands["add"] = add;
    commands["remove"] = removeObj;
    commands["search"] = search;
    commands["print"] = print;
    commands["load"] = load;
    commands["save"] = save;
    commands["help"] = help;
    commands["exit"] = exit;
    std::cout << "Write 'help' to get commands" << std::endl;

    while (true) {
        char prompt[STRSIZE] = {};
        std::cout << "Car list menu# ";
        std::cin.getline(prompt, STRSIZE);
        if (std::cin.fail()) {
            std::cout << "error!1!";
            break;
        }
        if (std::strlen(prompt) == 0) {
            continue;
        }
        char promptCopy[STRSIZE];
        std::strcpy(promptCopy, prompt);
        const char* command = std::strtok(promptCopy, wordSep);
        const char* line = promptCopy + std::strlen(command) + 1;
        
        if (commands.find(command) == commands.end()) {
            std::system(prompt);
            continue;
        }
        commands[command](line);
    }
}
