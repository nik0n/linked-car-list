#include <iostream>
#include <cstring>

#include "SportsCar.h"


SportsCar::SportsCar(
        const char *brand,
        const char *model,
        int maxSpeed,
        int mass,
        float length,
        bool autopilot,
        const char *turbineType
) : PassengerCar(SPORTSCAR, brand, model, maxSpeed, mass, length, autopilot) {
    strcpy(this->turbineType, turbineType);
    classname = SPORTSCAR;
}

SportsCar::SportsCar() {
    strcpy(this->turbineType, turbineType);
    classname = SPORTSCAR;
}

void SportsCar::print(std::ostream& ostream) const {
    PassengerCar::print(ostream);
    ostream << "\n" << "TurbineType: " << turbineType << std::endl;
}

void SportsCar::save(std::ofstream& fout) {
    PassengerCar::save(fout);
    fout.write(reinterpret_cast<char*>(&turbineType), sizeof(turbineType));
}

void SportsCar::load(std::ifstream& fin) {
    PassengerCar::load(fin);
    fin.read(reinterpret_cast<char*>(&turbineType), sizeof(turbineType));
}

const char* SportsCar::getTurbineType() const {
    return turbineType;
}
