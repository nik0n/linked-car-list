#include <iostream>
#include <cstring>

#include "FreightCar.h"

FreightCar::FreightCar(
        ClassName classname,
        const char *brand,
        const char *model,
        int maxSpeed,
        int mass,
        float length,
        bool hingedCab
) : Car(classname, brand, model, maxSpeed, mass, length), hingedCab(hingedCab) {
    classname = FREIGHTCAR;
}

FreightCar::FreightCar()
: hingedCab(false) {
    classname = FREIGHTCAR;
}

void FreightCar::print(std::ostream& ostream) const {
    Car::print(ostream);
    ostream << "\n" << "HingedCab: " << hingedCab;
};

void FreightCar::save(std::ofstream& fout) {
    Car::save(fout);
    fout.write(reinterpret_cast<char*>(&hingedCab), sizeof(hingedCab));
}

void FreightCar::load(std::ifstream& fin) {
    Car::load(fin);
    fin.read(reinterpret_cast<char*>(&hingedCab), sizeof(hingedCab));
}

bool FreightCar::getHingedCab() const {
    return hingedCab;
}
