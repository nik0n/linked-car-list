#pragma once


const size_t STRSIZE = 150;
enum ClassName {
    CAR,
    FREIGHTCAR,
    PASSENGERCAR,
    TRACTOR,
    CARAVAN,
    SPORTSCAR,
    GENERALPURPOSECAR,
};
const char wordSep[] = " \n\t\r";
