#pragma once

#include <cstdint>
#include <ostream>
#include <fstream>
#include "config.h"

class Car {
protected:
    ClassName classname;
    char brand[STRSIZE];
    char model[STRSIZE];
    int maxSpeed;
    int mass;
    float length;
public:
    Car(ClassName classname, const char *brand, const char *model, int maxSpeed,
        int mass, float length);
    Car();
    friend std::ostream& operator<<(std::ostream&, const Car&);
    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);
    const char *getBrand() const;
    const char *getModel() const;
    int getMaxSpeed() const;
    int getMass() const;
    float getLength() const;
    ClassName getClassname() const;
};
