#include <iostream>
#include <cstring>

#include "GeneralPurposeCar.h"

GeneralPurposeCar::GeneralPurposeCar(
        const char *brand,
        const char *model,
        int maxSpeed,
        int mass,
        float length,
        bool autopilot,
        const char *powerType
) : PassengerCar(GENERALPURPOSECAR, brand, model, maxSpeed, mass, length, autopilot) {
    strcpy(this->powerType, powerType);
    classname = GENERALPURPOSECAR;
}

GeneralPurposeCar::GeneralPurposeCar() {
    strcpy(powerType, "Electric");
    classname = GENERALPURPOSECAR;
}

void GeneralPurposeCar::print(std::ostream& ostream) const {
    PassengerCar::print(ostream);
    ostream
            << "\n" << "PowerType: " << powerType << std::endl;
}

void GeneralPurposeCar::save(std::ofstream& fout) {
    PassengerCar::save(fout);
    fout.write(reinterpret_cast<char*>(&powerType), sizeof(powerType));
}

void GeneralPurposeCar::load(std::ifstream& fin) {
    PassengerCar::load(fin);
    fin.read(reinterpret_cast<char*>(&powerType), sizeof(powerType));
}

const char* GeneralPurposeCar::getPowerType() const {
    return powerType;
}
