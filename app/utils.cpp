#include <fstream>
#include <iostream>
#include <cstring>
#include <limits>

#include "utils.h"
#include "config.h"
#include "Tractor.h"
#include "Caravan.h"
#include "GeneralPurposeCar.h"
#include "SportsCar.h"

Car* read_from_file_smart(const char* filename, size_t& offset) {

    ClassName classname;
    std::ifstream fin(filename, std::ios::binary);
    fin.seekg(offset);
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.seekg(offset);

    Car* vehicle;
    switch (classname) {
        case CARAVAN:
            vehicle = new Caravan;
            break;
        case TRACTOR:
            vehicle = new Tractor;
            break;
        case SPORTSCAR:
            vehicle = new SportsCar;
            break;
        case GENERALPURPOSECAR:
            vehicle = new GeneralPurposeCar;
            break;
        default:
            return nullptr;
    }

    vehicle->load(fin);
    
    if (fin.fail()) {
        return nullptr;
    }

    offset = fin.tellg();
    fin.close();

    return vehicle;
}


void parseWords(const char string[], char words[][STRSIZE]) {
    size_t stringLength = std::strlen(string);
    char *stringCopy = new char[stringLength + 4]();
    std::strcpy(stringCopy, string);

    char *item = std::strtok(stringCopy, wordSep);
    for (size_t i = 0; item; i++) {
        std::strcpy(words[i], item);
        item = std::strtok(NULL, wordSep);
    }

    delete[] stringCopy;
}

void clearcin() {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
