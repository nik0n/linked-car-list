#pragma once

#include <iostream>
#include <cstring>

#include "Node.h"
#include "utils.h"


template<typename T>
class List {
private:
    Node<T>* first = nullptr;
    Node<T>* last = nullptr;
public:
    Node<T>* getHead() {
        return first;
    }

    ~List() {
        removeAll();
    }

    void append(T* data) {
        Node<T>* node = new Node<T>(data);
        if (first == nullptr) {
            first = node;
            last = first;
        } else {
            last->set_next(node);
            last = node;
        }
    }

    void printAll() {
        size_t i = 1;
        for (Node<T>* node = first; node != nullptr; node = node->get_next(), i++) {
            T* vehicle = node->get_value();
            std::cout << i << ". " << *vehicle;
        }
    }

    void saveAll(const char* filename) {
        std::ofstream fout(filename, std::ios::binary);
        for (Node<T>* node = first; node != nullptr; node = node->get_next()) {
            T* vehicle = node->get_value();
            vehicle->save(fout);
        }
        fout.close();
    }

    void loadFrom(const char* filename) {
        size_t offset = 0;
        while (true) {
            T* vehicle = read_from_file_smart(filename, offset);
            if (vehicle == nullptr) {
                break;
            }
            append(vehicle);
        }
    }

    T* search(const char* name) {
        for (Node<T>* node = first; node != nullptr; node = node->get_next()) {
            T* vehicle = node->get_value();
            if (std::strcmp(name, vehicle->getBrand()) == 0) {
                return vehicle;
            }
        }
        return nullptr;
    }

    void remove(const char* brand, size_t count=0) {
        size_t deleted = 0;

        T empty_prehead_value;
        Node<T> prehead(&empty_prehead_value);
        prehead.set_next(first);

        for (Node<T>* node = &prehead; node != nullptr && node->get_next() != nullptr;) {
            Node<T>* next = node->get_next();

            T* value = next->get_value();
            if (std::strcmp(value->getBrand(), brand) == 0) {
                if (node == &prehead) {
                    first = first->get_next();
                    prehead.set_next(first);
                } else {
                    node->set_next(next->get_next());
                }
                delete next;
                if (count > 0) {
                    deleted++;
                    if (deleted == count) {
                        break;
                    }
                }
            } else {
                node = node->get_next();
            }
        }
    }

    void removeAll() {
         while (first != nullptr) {
            Node<T>* current = first;
             first = first->get_next();
            delete current;
        }
    }
};
