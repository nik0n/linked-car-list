#pragma once

#include "PassengerCar.h"
#include "config.h"


class SportsCar : public PassengerCar {
protected:
    char turbineType[STRSIZE]{};
public:
    SportsCar(const char*, const char*, int, int, float, bool, const char*);
    SportsCar();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    const char *getTurbineType() const;
};
