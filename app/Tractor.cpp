#include <cstring>
#include <iostream>
#include "Tractor.h"


Tractor::Tractor(
        const char *brand,
        const char *model,
        int maxSpeed,
        int mass,
        float length,
        bool hingedCab,
        const char *typeOfTrailer
) : FreightCar(TRACTOR, brand, model, maxSpeed, mass, length, hingedCab) {
    strcpy(this->typeOfTrailer, typeOfTrailer);
    classname = TRACTOR;
}


Tractor::Tractor() {
    classname = TRACTOR;
    strcpy(typeOfTrailer, "void");
}


void Tractor::print(std::ostream& ostream) const {
    FreightCar::print(ostream);
    ostream << "\n" << "TypeOfTrailer: " << typeOfTrailer << std::endl;
}


void Tractor::save(std::ofstream& fout) {
    FreightCar::save(fout);
    fout.write(typeOfTrailer, sizeof(typeOfTrailer));
}


void Tractor::load(std::ifstream& fin) {
    FreightCar::load(fin);
    fin.read(typeOfTrailer, sizeof(typeOfTrailer));
}


const char* Tractor::getTypeOfTrailer() const {
    return typeOfTrailer;
}
