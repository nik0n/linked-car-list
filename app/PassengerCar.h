#pragma once

#include "config.h"
#include "Car.h"


class PassengerCar : public Car {
protected:
    bool autopilot{};
public:
    PassengerCar(ClassName classname, const char*, const char*, int, int, float, bool);
    PassengerCar();

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    bool getAutopilot() const;
};
