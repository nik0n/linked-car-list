#include <cstdint>
#include <iostream>
#include <cstring>

#include "Car.h"

Car::Car(
    ClassName classname,
    const char *brand,
    const char *model,
    int maxSpeed,
    int mass,
    float length
) : classname(classname),  maxSpeed(maxSpeed), mass(mass), length(length) {
    strcpy(this->brand, brand);
    strcpy(this->model, model);
}

Car::Car() : classname(CAR), maxSpeed(120), mass(1500), length(4.7)  {
    strcpy(brand, "Brand");
    strcpy(model, "Model");
}

const char* Car::getBrand() const {
    return brand;
}

const char* Car::getModel() const {
    return model;
}

int Car::getMaxSpeed() const {
    return maxSpeed;
}

int Car::getMass() const {
    return mass;
}

float Car::getLength() const {
    return length;
}

ClassName Car::getClassname() const {
    return classname;
}

void Car::print(std::ostream& ostream) const {
    ostream
            << "Current auto" << "\n"
            << brand << " " << model << "\n"
            << "Mass: "<< mass << "\n"
            << "Max speed: " << maxSpeed << "\n"
            << "Length: " << length;
}

std::ostream& operator<<(std::ostream& ostream, const Car& player) {
    player.print(ostream);
    return ostream;
}

void Car::save(std::ofstream& fout) {
    fout.write(reinterpret_cast<char*>(&classname), sizeof(classname));
    fout.write(brand, sizeof(brand));
    fout.write(model, sizeof(model));
    fout.write(reinterpret_cast<char*>(&maxSpeed), sizeof(maxSpeed));
    fout.write(reinterpret_cast<char*>(&mass), sizeof(mass));
    fout.write(reinterpret_cast<char*>(&length), sizeof(length));
}

void Car::load(std::ifstream& fin) {
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.read(brand, sizeof(brand));
    fin.read(model, sizeof(model));
    fin.read(reinterpret_cast<char*>(&maxSpeed), sizeof(maxSpeed));
    fin.read(reinterpret_cast<char*>(&mass), sizeof(mass));
    fin.read(reinterpret_cast<char*>(&length), sizeof(length));
}
