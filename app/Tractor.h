#pragma once

#include "config.h"
#include "FreightCar.h"

class Tractor : public FreightCar {
protected:
    char typeOfTrailer[STRSIZE]{};
public:
    Tractor(const char*, const char*, int, int, float, bool, const char*);
    Tractor();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);
    
    const char *getTypeOfTrailer() const;
};
