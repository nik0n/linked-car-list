#include <iostream>
#include <cstring>

#include "Caravan.h"


Caravan::Caravan(
        const char *brand,
        const char *model,
        int maxSpeed,
        int mass,
        float length,
        bool hingedCab,
        const char *typeOfSale
) : FreightCar(CARAVAN, brand, model, maxSpeed, mass, length, hingedCab) {
    strcpy(this->typeOfSale, typeOfSale);
    classname = CARAVAN;
}

Caravan::Caravan() {
    strcpy(typeOfSale, "StreetFood");
    classname = CARAVAN;
}

void Caravan::print(std::ostream& ostream) const {
    FreightCar::print(ostream);
    ostream << "\n" << "TypeOfSale: " << typeOfSale << std::endl;
}

void Caravan::save(std::ofstream& fout) {
    FreightCar::save(fout);
    fout.write(reinterpret_cast<char*>(typeOfSale), sizeof(typeOfSale));
}

void Caravan::load(std::ifstream& fin) {
    FreightCar::load(fin);
    fin.read(reinterpret_cast<char*>(typeOfSale), sizeof(typeOfSale));
}

const char* Caravan::getTypeOfSale() const {
    return typeOfSale;
}
